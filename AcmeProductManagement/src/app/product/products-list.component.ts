import { Component, OnInit } from '@angular/core';
import { IProduct } from './product';
import { ProductService } from './product.service';

@Component({
  selector: 'pm-products',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {

  pageHeader:string= 'Product List';
  imageWidth:number=50;
  imageMargin:number=2;
  showImage:boolean=false;
  errorMessage:string;
  _listFilter:string;
  get listFilter():string {
      return this._listFilter;
  }
  set listFilter(value:string){
      this._listFilter=value;
      this.filteredProducts=this.listFilter? this.performFilter(this.listFilter):this.products;
  }
  filteredProducts: IProduct[];
  products:IProduct[]=[];
  constructor(private productService:ProductService) { 
     
  }

  ngOnInit() {
     this.productService.getProducts().subscribe(
       products => {
         this.products=products,
         this.filteredProducts=this.products
       },
       error=> this.errorMessage=<any>error
     );
    
    this.listFilter='';
  }

  toggleImage():void{
    this.showImage=!this.showImage;
  }

  performFilter(filterBy: string):IProduct[]{
    filterBy=filterBy.toLocaleLowerCase();
    return this.products.filter((product:IProduct)=>
                product.productName.toLocaleLowerCase().indexOf(filterBy) !== -1);

  }

  onRatingClicked(message: string):void{
    this.pageHeader='Product List: '+ message;
  }
  

}

import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import { ProductDetailGuard } from './product-detail.guard';
import { ProductDetailComponent } from './product-detail.component';
import { ProductsListComponent } from './products-list.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [ 
    ProductsListComponent,
    ProductDetailComponent
    ],
  imports: [   
    RouterModule.forChild([
      {path:'products', component: ProductsListComponent},
      {path: 'products/:id', canActivate: [ProductDetailGuard], component:ProductDetailComponent}    
    ]),
    SharedModule
  ]
})
export class ProductModule { }

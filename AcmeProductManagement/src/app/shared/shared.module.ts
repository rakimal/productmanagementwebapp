import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from '@angular/forms'
import { ConvertToSpacesPipe } from '../shared/convert-to-spaces.pipe';
import { StarsComponent } from '../shared/stars.component';

@NgModule({
  declarations: [ConvertToSpacesPipe,
    StarsComponent],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule    
  ],
  exports: [
    StarsComponent,
    ConvertToSpacesPipe,
    CommonModule,
    BrowserModule,
    FormsModule
  ]
})
export class SharedModule { }

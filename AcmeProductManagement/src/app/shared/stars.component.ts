import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'pm-star',
  templateUrl: './stars.component.html',
  styleUrls: ['./stars.component.css']
})
export class StarsComponent implements OnInit {

  starWidth:number;
  @Input() rating:number;
  @Output() ratingClicked: EventEmitter<string> = new EventEmitter<string>();
  constructor() { }

  ngOnInit() {
   
  }

  ngOnChanges():void{
    this.starWidth= this.rating*75/5;
  }

  onClicked():void{
    this.ratingClicked.emit("The rating is " + this.rating);
  }
}
